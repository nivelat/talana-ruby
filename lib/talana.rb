require 'talana/version'
require 'talana/client'

module Talana
  class << self
    def client(user, password, server = '')
      Talana::Client.new(user, password, server)
    end
  end
end
