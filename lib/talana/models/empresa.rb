require 'talana/models/base'

module Talana
  class Empresa < Base
    attr_accessor :calendario_de_feriados,
                  :company_group,
                  :es_prueba,
                  :fecha_creacion,
                  :id,
                  :logo_web_publica,
                  :logo,
                  :nombre,
                  :orden_web_publica,
                  :pais,
                  :tags,
                  :tier,
                  :url,
                  :vigente

    def initialize(attributes)
      @calendario_de_feriados = attributes['calendarioDeFeriados']
      @company_group = attributes['company_group']
      @es_prueba = attributes['esPrueba']
      @fecha_creacion = attributes['fechaCreacion']
      @id = attributes['id']
      @logo = attributes['logo']
      @logo_web_publica = attributes['logoWebPublica']
      @nombre = attributes['nombre']
      @orden_web_publica = attributes['ordenWebPublica']
      @pais = attributes['pais']
      @tags = attributes['tags']
      @tier = attributes['tier']
      @url = attributes['url']
      @vigente = attributes['vigente']
    end
  end
end