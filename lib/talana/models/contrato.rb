require 'talana/models/base'
require 'talana/models/persona'
require 'talana/models/sucursal'
require 'talana/models/centro_costo'

module Talana
  class Contrato < Base
    attr_accessor :adscribe_a_seguro_cesantia_para_contratos_previos_a2002,
                  :afp,
                  :anexo,
                  :anticipo_pactado,
                  :apv_cuenta_dos,
                  :apv_cuenta_dos_moneda,
                  :apv_institucion,
                  :apv_moneda,
                  :apv_monto,
                  :apv_tipo,
                  :asignacion_zona_extrema,
                  :beneficios_info_adicional,
                  :cargo,
                  :centro_costo,
                  :clase_salarial,
                  :clausulas_adicionales,
                  :codigo,
                  :codigo_franquicia_sence,
                  :corresponde_asignacion_maternal,
                  :creado_por,
                  :deposito_convenido_moneda,
                  :deposito_convenido_monto,
                  :descripcion_del_cargo,
                  :desde,
                  :detalle_anexo_contrato,
                  :dias_adicionales_vacaciones,
                  :dias_administrativos,
                  :documento_es_contrato_o_anexo,
                  :empleado,
                  :empleado_details,
                  :empleador_razon_social,
                  :empresa,
                  :es_pensionado,
                  :external_reference,
                  :fecha_contratacion,
                  :fecha_creacion,
                  :fecha_de_contratacion_reconocida_para_anos_de_servicio,
                  :fecha_modificacion,
                  :finiquitado,
                  :grupos,
                  :hasta,
                  :horas_de_la_jornada,
                  :id,
                  :id_contrato,
                  :indemnizacion_sin_tope_anos,
                  :indemnizacion_sin_tope_renta,
                  :ine,
                  :isapre,
                  :jefe,
                  :jornada,
                  :mantiene_renta_liquida_licencia,
                  :meses_imponiblesreconocidos,
                  :meses_imponibles_reconocidos_desde,
                  :monto_pactado2_isapre,
                  :monto_pactado_isapre,
                  :monto_pactado_isapre_moneda,
                  :motivo_egreso,
                  :nivel_sence,
                  :paga_tres_primeros_dias_licencia,
                  :retencion_juducial_destinatario,
                  :rol_privado,
                  :sindicato,
                  :sucursal,
                  :sueldo_banco,
                  :sueldo_cuenta_corriente,
                  :sueldo_cuenta_corriente_tipo,
                  :sueldo_forma_pago,
                  :sueldo_patronal,
                  :sueldo_tipo_pago,
                  :tipo_contrato,
                  :tipo_contrato_details,
                  :tramo_asignacion_previsional,
                  :unidad_organizacional,
                  :unidad_organizacional_details,
                  :user_defined_fields,
                  :vacaciones_reconocido_desde,
                  :valor_hora_extra_pactada,
                  :zona_asignacion_previsional

    def initialize(attributes)
      @adscribe_a_seguro_cesantia_para_contratos_previos_a2002 = attributes['adscribeASeguroCesantiaParaContratosPreviosA2002']
      @afp = attributes['afp']
      @anexo = attributes['anexo']
      @anticipo_pactado = attributes['anticipoPactado']
      @apv_cuenta_dos = attributes['apvCuentaDos']
      @apv_cuenta_dos_moneda = attributes['apvCuentaDosMoneda']
      @apv_institucion = attributes['apvInstitucion']
      @apv_moneda = attributes['apvMoneda']
      @apv_monto = attributes['apvMonto']
      @apv_tipo = attributes['apvTipo']
      @asignacion_zona_extrema = attributes['asignacionZonaExtrema']
      @beneficios_info_adicional = attributes['beneficiosInfoAdicional']
      @cargo = attributes['cargo']
      @clase_salarial = attributes['claseSalarial']
      @clausulas_adicionales = attributes['clausulasAdicionales']
      @codigo = attributes['codigo']
      @codigo_franquicia_sence = attributes['codigoFranquiciaSence']
      @corresponde_asignacion_maternal = attributes['correspondeAsignacionMaternal']
      @creado_por = attributes['creadoPor']
      @deposito_convenido_moneda = attributes['depositoConvenidoMoneda']
      @deposito_convenido_monto = attributes['depositoConvenidoMonto']
      @descripcion_del_cargo = attributes['descripcionDelCargo']
      @desde = attributes['desde']
      @detalle_anexo_contrato = attributes['detalleAnexoContrato']
      @dias_adicionales_vacaciones = attributes['diasAdicionalesVacaciones']
      @dias_administrativos = attributes['diasAdministrativos']
      @documento_es_contrato_o_anexo = attributes['documentoEsContratoOAnexo']
      @empleado = attributes['empleado']
      @empleador_razon_social = attributes['empleadorRazonSocial']
      @empresa = attributes['empresa']
      @es_pensionado = attributes['esPensionado']
      @external_reference = attributes['externalReference']
      @fecha_contratacion = attributes['fechaContratacion']
      @fecha_creacion = attributes['fechaCreacion']
      @fecha_de_contratacion_reconocida_para_anos_de_servicio = attributes['fechaDeContratacionReconocidaParaAnosDeServicio']
      @fecha_modificacion = attributes['fechaModificacion']
      @finiquitado = attributes['finiquitado']
      @grupos = attributes['grupos']
      @hasta = attributes['hasta']
      @horas_de_la_jornada = attributes['horasDeLaJornada']
      @id = attributes['id']
      @id_contrato = attributes['idContrato']
      @indemnizacion_sin_tope_anos = attributes['indemnizacionSinTopeAnos']
      @indemnizacion_sin_tope_renta = attributes['indemnizacionSinTopeRenta']
      @ine = attributes['INE']
      @isapre = attributes['isapre']
      @jefe = attributes['jefe']
      @jornada = attributes['jornada']
      @mantiene_renta_liquida_licencia = attributes['mantieneRentaLiquidaLicencia']
      @meses_imponiblesreconocidos = attributes['mesesImponiblesreconocidos']
      @meses_imponibles_reconocidos_desde = attributes['mesesImponiblesReconocidosDesde']
      @monto_pactado2_isapre = attributes['montoPactado2Isapre']
      @monto_pactado_isapre = attributes['montoPactadoIsapre']
      @monto_pactado_isapre_moneda = attributes['montoPactadoIsapreMoneda']
      @motivo_egreso = attributes['motivoEgreso']
      @nivel_sence = attributes['nivelSence']
      @paga_tres_primeros_dias_licencia = attributes['pagaTresPrimerosDiasLicencia']
      @retencion_juducial_destinatario = attributes['retencionJuducialDestinatario']
      @rol_privado = attributes['rolPrivado']
      @sindicato = attributes['sindicato']
      @sueldo_banco = attributes['sueldoBanco']
      @sueldo_cuenta_corriente = attributes['sueldoCuentaCorriente']
      @sueldo_cuenta_corriente_tipo = attributes['sueldoCuentaCorrienteTipo']
      @sueldo_forma_pago = attributes['sueldoFormaPago']
      @sueldo_patronal = attributes['sueldoPatronal']
      @sueldo_tipo_pago = attributes['sueldoTipoPago']
      @tipo_contrato = attributes['tipoContrato']
      @tipo_contrato_details = attributes['tipoContratoDetails']
      @tramo_asignacion_previsional = attributes['tramoAsignacionPrevisional']
      @unidad_organizacional = attributes['unidadOrganizacional']
      @unidad_organizacional_details = attributes['unidadOrganizacionalDetails']
      @user_defined_fields = attributes['userDefinedFields']
      @vacaciones_reconocido_desde = attributes['vacacionesReconocidoDesde']
      @valor_hora_extra_pactada = attributes['valorHoraExtraPactada']
      @zona_asignacion_previsional = attributes['zonaAsignacionPrevisional']

      unless attributes['empleadoDetails'].nil?
        @empleado_details = Talana::Persona.new(attributes['empleadoDetails'])
      end

      unless attributes['sucursal'].nil?
        @sucursal = Talana::Sucursal.new(attributes['sucursal'])
      end

      unless attributes['centroCosto'].nil?
        @centro_costo = Talana::CentroCosto.new(attributes['centroCosto'])
      end
    end
  end
end
