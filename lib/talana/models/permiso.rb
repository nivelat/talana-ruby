require 'talana/models/base'

module Talana
  class Permiso < Base
    attr_accessor :nombre,
                  :vigente

    def initialize(attributes)
      @nombre = attributes['nombre']
      @vigente = attributes['vigente']
    end
  end
end