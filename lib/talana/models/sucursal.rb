require 'talana/models/base'

module Talana
  class Sucursal < Base
    attr_accessor :beacons,
                  :creado_por,
                  :desde,
                  :direccion_calle,
                  :direccion_ciudad,
                  :direccion_comuna,
                  :direccion_departamento,
                  :direccion_numero,
                  :empresa,
                  :external_reference,
                  :fecha_creacion,
                  :id,
                  :location,
                  :location_parseado,
                  :nombre,
                  :rango,
                  :telefono,
                  :vigente

    def initialize(attributes)
      @beacons = attributes['beacons']
      @creado_por = attributes['creadoPor']
      @desde = attributes['desde']
      @direccion_calle = attributes['direccionCalle']
      @direccion_ciudad = attributes['direccionCiudad']
      @direccion_comuna = attributes['direccionComuna']
      @direccion_departamento = attributes['direccionDepartamento']
      @direccion_numero = attributes['direccionNumero']
      @empresa = attributes['empresa']
      @external_reference = attributes['externalReference']
      @fecha_creacion = attributes['fechaCreacion']
      @id = attributes['id']
      @location = attributes['location']
      @location_parseado = attributes['location_parseado']
      @nombre = attributes['nombre']
      @rango = attributes['rango']
      @telefono = attributes['telefono']
      @vigente = attributes['vigente']
    end
  end
end