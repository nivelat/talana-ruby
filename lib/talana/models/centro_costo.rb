require 'talana/models/base'

module Talana
  class CentroCosto < Base
    attr_accessor :codigo,
                  :empresa,
                  :external_reference,
                  :id,
                  :nombre,
                  :parent,
                  :user_defined_fields,
                  :vigente

    def initialize(attributes)
      @codigo = attributes['codigo']
      @empresa = attributes['empresa']
      @external_reference = attributes['externalReference']
      @id = attributes['id']
      @nombre = attributes['nombre']
      @parent = attributes['parent']
      @user_defined_fields = attributes['user_defined_fields']
      @vigente = attributes['vigente']
    end
  end
end