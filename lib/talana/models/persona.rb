require 'talana/models/base'
require 'talana/models/empresa'
require 'talana/models/permiso'

module Talana
  class Persona < Base
    attr_accessor :apellido_materno,
                  :apellido_paterno,
                  :detalles,
                  :email,
                  :empresa,
                  :external_reference,
                  :fecha_creacion,
                  :fecha_nacimiento,
                  :id,
                  :nacionalidad,
                  :nombre,
                  :permisos,
                  :rut,
                  :sexo,
                  :username

    def initialize(attributes)
      @apellido_materno = attributes['apellidoMaterno']
      @apellido_paterno = attributes['apellidoPaterno']
      @email = attributes['email']
      @external_reference = attributes['externalReference']
      @fecha_creacion = attributes['fechaCreacion']
      @fecha_nacimiento = attributes['fechaNacimiento']
      @id = attributes['id']
      @nacionalidad = attributes['nacionalidad']
      @nombre = attributes['nombre']
      @rut = attributes['rut']
      @sexo = attributes['sexo']
      @username = attributes['username']

      unless attributes['empresa'].nil?
        @empresa = Talana::Empresa.new(attributes['empresa'])
      end

      @permisos = (attributes['permisos'] || []).map do |permiso|
        Talana::Permiso.new(permiso) if permiso.is_a? Hash
      end

      @detalles = (attributes['detalles'] || []).map do |detalle|
        Talana::Persona::Detalle.new(detalle)
      end
    end

    class Detalle
      attr_accessor :alergias,
                    :celular,
                    :colegio,
                    :contactos_de_emergencia,
                    :direccion_calle,
                    :direccion_ciudad,
                    :direccion_comuna,
                    :direccion_departamento,
                    :direccion_numero,
                    :discapacidades,
                    :email_personal,
                    :email,
                    :enfermedades_cronicas,
                    :estado_civil,
                    :fecha_creacion,
                    :foto,
                    :id,
                    :idiomas,
                    :institucion_estudios_superiores,
                    :medicamentos_permanentes,
                    :nivel_educacional,
                    :observaciones,
                    :profesion,
                    :sangre,
                    :telefono,
                    :valido_desde

        def initialize(attributes)
          @alergias = attributes['alergias']
          @celular = attributes['celular']
          @colegio = attributes['colegio']
          @contactos_de_emergencia = attributes['contactosDeEmergencia']
          @direccion_calle = attributes['direccionCalle']
          @direccion_ciudad = attributes['direccionCiudad']
          @direccion_comuna = attributes['direccionComuna']
          @direccion_departamento = attributes['direccionDepartamento']
          @direccion_numero = attributes['direccionNumero']
          @discapacidades = attributes['discapacidades']
          @email = attributes['email']
          @email_personal = attributes['emailPersonal']
          @enfermedades_cronicas = attributes['enfermedades_cronicas']
          @estado_civil = attributes['estadoCivil']
          @fecha_creacion = attributes['fechaCreacion']
          @foto = attributes['foto']
          @id = attributes['id']
          @idiomas = attributes['idiomas']
          @institucion_estudios_superiores = attributes['institucionEstudiosSuperiores']
          @medicamentos_permanentes = attributes['medicamentos_permanentes']
          @nivel_educacional = attributes['nivelEducacional']
          @observaciones = attributes['observaciones']
          @profesion = attributes['profesion']
          @sangre = attributes['sangre']
          @telefono = attributes['telefono']
          @valido_desde = attributes['validoDesde']
        end
    end
  end
end