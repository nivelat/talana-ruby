require 'faraday'

require 'talana/api/personas'
require 'talana/api/contratos'
require 'talana/api/contratos_v2'
require 'talana/api/contratos_resumidos'
require 'talana/api/cargos'

module Talana
  class Client
    include Talana::API::Personas
    include Talana::API::Contratos

    # Implementados pero no en uso
    #
    # include Talana::API::ContratosV2
    # include Talana::API::ContratosResumidos
    # include Talana::API::Cargos

    # No implementados aún
    #
    # include Talana::API::Vacaciones
    # include Talana::API::VacacionesResumidos
    # include Talana::API::Ausentismos
    # include Talana::API::AusentismosResumidos
    # include Talana::API::ProrrateoCentroDeCosto
    # include Talana::API::CentralizacionContable
    # include Talana::API::AsignacionItemsDePago
    # include Talana::API::Documentos
    # include Talana::API::DiasAdministrativos
    # include Talana::API::DiasAdministrativosResumidos
    # include Talana::API::EnrolamientoFirmaDigital
    # include Talana::API::SolicitudesFirmaDigital
    # include Talana::API::Turnos
    # include Talana::API::DiasTurnosSemanal
    # include Talana::API::DiasTurnosManual
    # include Talana::API::AsignacionPersonasATurnos
    # include Talana::API::InyeccionDeMarcas

    DEFAULT_SERVER = 'https://talana.com'

    attr_reader :auth_token

    def initialize(user, password, server = '', conn = nil)
      @user = user
      @password = password
      @server = validate_server(server)
      @conn = conn
      @auth_token = nil
    end

    def get_token
      body = { username: @user, password: @password }
      response = request(:post, '/es/api/api-token-auth/', body)

      @auth_token = response['token']
      @_client = nil

      response
    end

    private

    def request(method, path, body = nil)
      client.public_send(method, path, body).body
    end

    def auth_request(method, path, body = nil)
      get_token if @auth_token.nil?
      request(method, path, body)
    end

    def validate_server(server)
      expression = /^((http|https):\/\/)?((\w+\.)?[a-zA-Z0-9]+\.[a-zA-Z0-9]{2,3}\/(es)\/(api)\/([a-zA-Z0-9\_\-\.]+\/)?([a-zA-Z0-9\_\-\.]+\/)?$)/

      return server if server.match(expression)
      return DEFAULT_SERVER
    end

    def client
      @_client = @conn || @_client || ::Faraday.new(@server) do |connection|
        connection.request :json
        connection.request :url_encoded
        connection.request :authorization, 'Token', @auth_token unless @auth_token.nil?

        connection.response :json
        connection.response :logger, nil, { headers: true, bodies: true }

        connection.adapter Faraday.default_adapter
      end
    end
  end
end