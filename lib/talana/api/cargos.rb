module Talana
  module API
    # Cargos
    #
    # El maestro de cargos de los trabajadores
    #
    module Cargos
      # Obtiene el listado de cargos.
      #
      # @return [Hash] Response from API.
      def cargos
        auth_request(:get, '/es/api/job-title/')
      end
    end
  end
end