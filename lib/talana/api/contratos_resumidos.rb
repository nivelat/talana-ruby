module Talana
  module API
    # Contratos Resumidos
    #
    # Los contratos de los trabajadores y sus condiciones contractuales, muestra
    # los contratos y condiciones activas al día de hoy.
    #
    module ContratosResumidos
      # Obtiene el listado de contratos.
      #
      # @return [Hash] Response from API.
      def contratos_resumidos
        auth_request(:get, '/es/api/contracts-resumed/')
      end

      # Obtiene el listado de contratos para una persona.
      #
      # @param id [Integer] ID de la persona.
      #
      # @return [Hash] Response from API.
      def contrato_persona_resumido(id)
        auth_request(:get, "/es/api/contracts-resumed/?empleado=#{id}")
      end
    end
  end
end