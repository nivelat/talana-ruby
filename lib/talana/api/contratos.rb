require 'talana/models/contrato'
module Talana
  module API
    # Contratos
    #
    # Servicio que permite obtener los contratos de los trabajadores y sus
    # condiciones contractuales. Muestra los contratos y condiciones activas al
    # día de hoy.
    #
    module Contratos
      # Obtiene el listado de contratos.
      #
      # @return [Hash] Response from API.
      def contratos
        result = auth_request(:get, '/es/api/contrato/')
        result.map do |c|
          Talana::Contrato.new(c)
        end
      end

      # Obtiene el listado de contratos para una persona.
      #
      # @param id [Integer] ID de la persona.
      #
      # @return [Hash] Response from API.
      def contrato_persona(id)
        result = auth_request(:get, "/es/api/contrato/?empleado=#{id}")
        Talana::Contrato.new(result)
      end
    end
  end
end