module Talana
  module API
    # Contratos V2
    #
    # Esta API permite obtener los contratos de los trabajadores y sus
    # condiciones contractuales. Por defecto, entrega el último contrato
    # vigente de cada contratación en un periodo en específico.
    #
    module ContratosV2
      # Obtiene el listado de contratos V2.
      #
      # @return [Hash] Response from API.
      def contratos_v2
        auth_request(:get, '/es/api/remuneraciones/contract/')
      end

      # Obtiene el listado de contratos para una persona V2.
      #
      # @param id [Integer] ID de la persona.
      #
      # @return [Hash] Response from API.
      def contrato_persona_v2(id)
        auth_request(:get, "/es/api/remuneraciones/contract/?empleado=#{id}")
      end
    end
  end
end