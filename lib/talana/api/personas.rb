require 'talana/models/persona'

module Talana
  module API
    # Persona
    #
    # Permite obtener la información de las personas, cada persona puede tener
    # uno o más contratos a lo largo de la relación laboral con una empresa,
    # pero sólo tendrá una ficha.
    #
    module Personas
      # Obtiene el listado de personas.
      #
      # @return [Hash] Response from API.
      def personas
        result = auth_request(:get, '/es/api/persona/')
        result.map { |p| Talana::Persona.new(p) }
      end

      # Obtiene el detalle de una persona.
      #
      # @param id [Integer] ID de la persona.
      #
      # @return [Hash] Response from API.
      def persona(id)
        result = auth_request(:get, "/es/api/persona/#{id}/")
        Talana::Persona.new(result)
      end
    end
  end
end