# Talana Ruby Client

Cliente para la API de Talana. https://ayuda.talana.com/documentacion/pack-de-bienvenida/integraciones/integraci%C3%B3n-web-service-y-api/

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'talana-ruby', require: false
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install talana-ruby

## Usage

### Configure your client

Configure the client, you can use any user and password for your Talana account following the steps described in the docs https://ayuda.talana.com/documentacion/pack-de-bienvenida/integraciones/integraci%C3%B3n-web-service-y-api/

```ruby
require 'talana'

client = Talana::Client.new('user', 'password')

# (Optional) Set a custom server for the API calls
client = Talana::Client.new('user', 'password', 'https://mycompany.com/api/')

# (Optional) Set a specific Faraday connection, mainly for testing purposes
client = Talana::Client.new('user', 'password', nil, Faraday.new)
```

### Resources

Resources this client supports:

```
https://talana.com/es/api/persona/
https://talana.com/es/api/persona/{id}/
```

### Examples

#### Personas

```ruby
# Get employee directory
personas = client.personas

# Get employee information (display name and work email)
id = 1234567890
persona = client.persona(id)

# Talana::Persona model
persona.id                 # [Integer] 123
persona.fecha_creacion     # [String] '2016-09-27T15:59:08.089222-03:00'
persona.rut                # [String] '12345678-K'
persona.nombre             # [String] 'Some'
persona.apellido_paterno   # [String] 'User'
persona.apellido_materno   # [String] 'User'
persona.sexo               # [String] 'M' || 'F'
persona.fecha_nacimiento   # [String] '1953-03-09'
persona.nacionalidad       # [String] 'CL'
persona.username           # [String] 'username'
persona.email              # [String] 'some.user@company.com'
persona.external_reference # [String] ''
persona.empresa            # [Talana::Empresa]
persona.permisos           # [Talana::Permiso[]]
persona.detalles           # [Talana::Persona::Detalle[]]

# Talana::Empresa model
empresa = persona.empresa
empresa.id                     # [Integer] 8
empresa.fecha_creacion         # [String] '2016-09-26T10:59:50.854243-03:00'
empresa.nombre                 # [String] 'Empresa de Prueba'
empresa.vigente                # [Boolean] true
empresa.url                    # [String] ''
empresa.logo                   # [String] 'https://talana.com/media/talana.jpg'
empresa.logo_web_publica       # [String] ''
empresa.orden_web_publica      # [String] ''
empresa.es_prueba              # [Boolean] true
empresa.tier                   # [String] 'tier1'
empresa.tags                   # [String[]] ['mensajeria']
empresa.company_group          # [String] ''
empresa.calendario_de_feriados # [Integer] 2
empresa.pais                   # [String] 'CL'

# Talana::Permiso model
permiso = persona.permisos.first
persona.nombre  # [String] 'permitir_ingreso_talana'
persona.vigente # [Boolean] true

# Talana::Persona::Detalle model
detalle = persona.detalles.first
detalle.id                              # [Integer] 1174806
detalle.fecha_creacion                  # [String] '2021-04-23T11:18:31.141912-04:00'
detalle.valido_desde                    # [String] '2021-04-23'
detalle.email                           # [String] 'some.user@company.com'
detalle.email_personal                  # [String] 'some.user@company.com'
detalle.telefono                        # [String] '1234567890'
detalle.celular                         # [String] '1234567890',
detalle.foto                            # [String] ''
detalle.direccion_calle                 # [String] ''
detalle.direccion_numero                # [String] ''
detalle.direccion_departamento          # [String] ''
detalle.nivel_educacional               # [String] ''
detalle.colegio                         # [String] ''
detalle.institucion_estudios_superiores # [String] ''
detalle.profesion                       # [String] ''
detalle.observaciones                   # [String] ''
detalle.contactos_de_emergencia         # [String] ''
detalle.sangre                          # [String] ''
detalle.alergias                        # [String] ''
detalle.discapacidades                  # [String] ''
detalle.enfermedades_cronicas           # [String] ''
detalle.medicamentos_permanentes        # [String] ''
detalle.direccion_comuna                # [String] ''
detalle.direccion_ciudad                # [String] ''
detalle.estado_civil                    # [String] 'casado'
detalle.idiomas                         # [String] ''

```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
