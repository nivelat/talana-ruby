require_relative '../../test_helper'
require 'talana/models/persona'

class TalanaAPIPersonasTest < Minitest::Test
  def test_that_it_has_a_personas_method
    assert_respond_to test_client, :personas
  end

  def test_it_returns_a_persona_array
    stubs = Faraday::Adapter::Test::Stubs.new
    stubs.get('/es/api/persona/') { stub_persona_request_single_result }

    cli = test_client(stubs)

    personas = cli.personas

    assert_equal personas.count, 1

    personas.each do |persona|
      assert_kind_of Talana::Persona, persona
    end
  end

  def test_it_returns_an_empty_personas_array
    stubs = Faraday::Adapter::Test::Stubs.new
    stubs.get('/es/api/persona/') { stub_persona_request_empty }

    cli = test_client(stubs)
    assert_equal cli.personas, []
  end

  private

  def stub_persona_request_empty
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '[]'
    ]
  end

  def stub_persona_request_single_result
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '[{"id":921, "fechaCreacion":"2016-09-27T15:59:08.089222-03:00", "rut":"12345678-K", "nombre":"TEST USER", "apellidoPaterno":"TEST", "apellidoMaterno":"USER", "sexo":"F", "fechaNacimiento":"1953-03-09", "nacionalidad":"CL", "username":"testuser", "email":"test@user.com", "permisos":[{"nombre":"ver_asistencia_mobile", "vigente":true}, {"nombre":"permitir_ingreso_talana", "vigente":true}, {"nombre":"permitir_marcacion_mobile", "vigente":true}], "detalles":[{"id":2371, "fechaCreacion":"2016-09-27T15:59:08.102652-03:00", "validoDesde":"2016-09-27", "email":"test@user.com", "emailPersonal":"test@user.com", "telefono":"", "celular":"", "foto":null, "direccionCalle":"", "direccionNumero":"", "direccionDepartamento":"", "nivelEducacional":"", "colegio":"", "institucionEstudiosSuperiores":"", "profesion":"", "observaciones":"", "contactosDeEmergencia":"", "sangre":null, "alergias":null, "discapacidades":null, "enfermedades_cronicas":null, "medicamentos_permanentes":null, "direccionComuna":null, "direccionCiudad":null, "estadoCivil":"soltero", "idiomas":[]}], "externalReference":null, "empresa":{"id":8, "fechaCreacion":"2016-09-26T10:59:50.854243-03:00", "nombre":"Empresa de Prueba", "vigente":true, "url":"", "logo":"https://talana.com/media/talana.jpg", "logoWebPublica":null, "ordenWebPublica":null, "esPrueba":true, "tier":"tier1", "tags":["mensajeria"], "company_group":null, "calendarioDeFeriados":2, "pais":"CL"}}]'
    ]
  end
end
