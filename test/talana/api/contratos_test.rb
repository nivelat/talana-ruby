require_relative '../../test_helper'
require 'talana/models/contrato'

class TalanaAPIPersonasTest < Minitest::Test
  def test_that_it_has_a_contratos_method
    assert_respond_to test_client, :contratos
  end

  def test_it_returns_a_contratos_array
    stubs = Faraday::Adapter::Test::Stubs.new
    stubs.get('/es/api/contrato/') { stub_contrato_request_single_result }

    cli = test_client(stubs)

    contratos = cli.contratos

    assert_equal contratos.count, 1

    contratos.each do |contrato|
      assert_kind_of Talana::Contrato, contrato
    end
  end

  def test_it_returns_an_empty_contrato_array
    stubs = Faraday::Adapter::Test::Stubs.new
    stubs.get('/es/api/contrato/') { stub_contrato_request_empty }

    cli = test_client(stubs)
    assert_equal cli.contratos, []
  end

  private

  def stub_contrato_request_empty
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '[]'
    ]
  end

  def stub_contrato_request_single_result
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '[{"id":1,"idContrato":"fc879fae-89f9-4088-a9b2-bf73daec5a6d","empresa":1,"empleado":1,"empleadoDetails":{"id":2,"fechaCreacion":"2023-04-13T10:30:33.146191-04:00","rut":"1-9","nombre":"nombre","apellidoPaterno":"apellidoPaterno","apellidoMaterno":"apellidoMaterno","sexo":"M","fechaNacimiento":"1984-01-01","nacionalidad":"CL","username":"usernme","email":null,"permisos":[],"detalles":[ {"id":3,"foto":null,"idiomas":[],"fechaCreacion":"2023-04-13T10:30:33.335990-04:00","validoDesde":"2023-04-13","email":null,"emailPersonal":"email@dominio.com","telefono":"","celular":"1234567890","direccionCalle":"direccionCalle","direccionNumero":"123","direccionDepartamento":"123","nivelEducacional":"","colegio":"","institucionEstudiosSuperiores":"","profesion":"","observaciones":"","contactosDeEmergencia":"","sangre":null,"alergias":null,"discapacidades":null,"enfermedades_cronicas":null,"medicamentos_permanentes":null,"direccionComuna":385,"direccionCiudad":null,"estadoCivil":"casado" }  ],"externalReference":null,"empresa":{ "id":4, "fechaCreacion":"2021-09-29T15:09:28.370153-03:00", "nombre":"empresa", "vigente":true, "url":"", "logo":null, "logoWebPublica":null, "ordenWebPublica":null, "esPrueba":false, "tier":"tier1", "tags":[], "company_group":null, "calendarioDeFeriados":1, "pais":"CL"  },"userDefinedFields":[],"anexo":null},"codigo":"001","tipoContrato":1,"tipoContratoDetails":{ "id":7, "nombre":"Indefinido" },"empleadorRazonSocial":9045,"fechaCreacion":"2023-04-13T10:30:33.228682-04:00","fechaModificacion":"2023-05-05T16:39:59.723234-04:00","cargo":"GERENTE DE PLANIFICACION Y CONTROL DE GESTION","fechaContratacion":"2023-04-03","desde":"2023-04-03","hasta":null,"finiquitado":false,"unidadOrganizacional":null,"unidadOrganizacionalDetails":null,"sucursal":{"id":8,"empresa":1,"nombre":"sucursal","desde":"2022-05-19","fechaCreacion":"2022-05-19T16:51:31.761665-04:00","creadoPor":1,"vigente":true,"direccionCalle":"direccionCalle","direccionNumero":"123","direccionDepartamento":"123","direccionComuna":"comuna","direccionCiudad":"ciudad","telefono":"","externalReference":null,"location":"POINT (-70 -33)","location_parseado":{ "lat":-33, "lng":-70 },"rango":30,"beacons":[]},"grupos":[1],"motivoEgreso":null,"anexo":null,"centroCosto":{"id":9,"parent":null,"empresa":1,"codigo":"123","nombre":"centrocosto","externalReference":null,"vigente":true,"user_defined_":{}},"horasDeLaJornada":0,"codigoFranquiciaSence":"","nivelSence":null,"INE":null,"sindicato":null,"jefe":1,"esPensionado":"N","tramoAsignacionPrevisional":null,"zonaAsignacionPrevisional":null,"correspondeAsignacionMaternal":false,"isapre":1,"montoPactadoIsapre":1,"montoPactadoIsapreMoneda":"UF","montoPactado2Isapre":null,"afp":1,"sueldoFormaPago":"Transferencia","sueldoBanco":1,"sueldoCuentaCorriente":"123","sueldoCuentaCorrienteTipo":"Cuenta Corriente","sueldoTipoPago":"mensual","valorHoraExtraPactada":0.0,"mesesImponiblesreconocidos":0,"mesesImponiblesReconocidosDesde":null,"vacacionesReconocidoDesde":null,"anticipoPactado":0.0,"fechaDeContratacionReconocidaParaAnosDeServicio":null,"pagaTresPrimerosDiasLicencia":false,"mantieneRentaLiquidaLicencia":false,"diasAdministrativos":0,"indemnizacionSinTopeAnos":false,"indemnizacionSinTopeRenta":false,"diasAdicionalesVacaciones":0,"descripcionDelCargo":null,"clausulasAdicionales":null,"detalleAnexoContrato":null,"documentoEsContratoOAnexo":"contrato","claseSalarial":null,"rolPrivado":true,"userDefinedFields":{},"externalReference":null,"asignacionZonaExtrema":0.0,"adscribeASeguroCesantiaParaContratosPreviosA2002":false,"sueldoPatronal":false,"depositoConvenidoMonto":0.0,"depositoConvenidoMoneda":"UF","retencionJuducialDestinatario":null,"apvMonto":0.0,"apvMoneda":"UF","apvInstitucion":null,"apvTipo":"B","apvCuentaDos":0.0,"apvCuentaDosMoneda":"UF","beneficiosInfoAdicional":null,"creadoPor":null}]'
    ]
  end
end
