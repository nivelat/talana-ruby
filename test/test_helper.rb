$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require "talana"

require "minitest/autorun"

def test_client(stubs = nil)
  ::Talana::Client.new('usr', 'pass', '', test_conn(stubs))
end

def test_conn(stubs = nil)
  stubs ||= Faraday::Adapter::Test::Stubs.new

  stubs.post('http:/es/api/api-token-auth/') do
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '{"token":"abc"}'
    ]
  end

  Faraday.new do |connection|
    connection.adapter :test, stubs
    connection.request :json
    connection.request :url_encoded
    connection.response :json, content_type: /.*/
  end
end