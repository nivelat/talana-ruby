require "test_helper"

class TalanaTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Talana::VERSION
  end
  
  def test_that_it_has_an_api_version
    refute_nil ::Talana::API_VERSION
  end

  def test_the_client_returns_a_talana_client
    assert_kind_of ::Talana::Client, ::Talana.client('usr', 'pass')
  end
end
